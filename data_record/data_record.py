from abc import ABCMeta


class DataRecord(object):

    __metaclass__ = ABCMeta
