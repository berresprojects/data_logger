from abc import ABCMeta


class DataLoggerConfiguration(object):

    __metaclass__ = ABCMeta

    def __init__(self, capture=list()):
        """
        :type capture: list[str]

        """
        self.capture = capture

    @classmethod
    def from_json(cls, json):
        return cls(**json)
