from csv import DictWriter
from unittest import TestCase

from datetime import datetime
from mock import MagicMock, patch

from data_fields.data_fields import DataFields
from data_logger import DataLogger
from data_logger_configuration import DataLoggerConfiguration
from data_record.data_record import DataRecord
from tests.test_data_fields.test_data_fields import ConcreteDataFields


class ConcreteDataLogger(DataLogger):

    @property
    def log_file_name(self):
        return "test"


class TestDataLogger(TestCase):

    @patch('data_logger.DictWriter', autospec=True)
    @patch('data_logger.open', create=True)
    @patch('data_logger.datetime')
    def test_init(self, mock_datetime, _mock_open, mock_dict_writer):
        mock_datetime.utcnow.return_value = datetime(2017, 12, 1, 15, 45, 45)
        mock_dict_writer_instance = MagicMock(spec=DictWriter)
        mock_dict_writer.return_value = mock_dict_writer_instance

        ConcreteDataLogger(ConcreteDataFields, DataLoggerConfiguration(["test1", "test2"]))

        _mock_open.called_once_with('results/DATE_1_12_2017_15_45_45_test.csv', 'w')
        mock_dict_writer.assert_called_once_with(_mock_open.return_value, fieldnames=["test1",
                                                                                      "test2"],
                                                 delimiter=";")
        mock_dict_writer_instance.writeheader.assert_called_once()

    @patch('data_logger.DictWriter', autospec=True)
    @patch('data_logger.open', create=True)
    def test_capture(self, _, mock_dict_writer):
        mock_dict_writer_instance = MagicMock(spec=DictWriter)
        mock_dict_writer.return_value = mock_dict_writer_instance
        data_logger = ConcreteDataLogger(ConcreteDataFields, DataLoggerConfiguration(["test1", "test2"]))
        data_record = MagicMock(spec=DataRecord)

        data_logger.capture(data_record)

        mock_dict_writer_instance.writerow.assert_called_once_with(dict())
