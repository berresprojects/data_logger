from unittest import TestCase

from mock import MagicMock, patch

from data_fields.data_field_handler.data_field_handler import DataFieldHandler
from data_fields.data_fields import DataFields
from data_record.data_record import DataRecord


class ConcreteDataFields(DataFields):
    def __init__(self, parameters_captured):
        self.mock1 = MagicMock()
        self.mock1.return_value = MagicMock(spec=DataFieldHandler)
        self.mock2 = MagicMock()
        self.mock2.return_value = MagicMock(spec=DataFieldHandler)
        super(ConcreteDataFields, self).__init__(parameters_captured)

    def get_data_field_handlers(self):
        return dict(
            test1=self.mock1,
            test2=self.mock2
        )


class TestDataFields(TestCase):

    def test_create_data_field_handler(self):
        data_fields = ConcreteDataFields(["test1", "test2"])

        data_fields.mock1.assert_called_once_with("test1", None)
        data_fields.mock2.assert_called_once_with("test2", data_fields.mock1.return_value)

    @patch("data_fields.data_fields.DummyDataFieldHandler")
    def test_create_data_field_handler_faulty_data_field(self, mock_dummy_data_field_handler):
        data_fields = ConcreteDataFields(["test1", "test2", "test3"])

        data_fields.mock1.assert_called_once_with("test1", None)
        data_fields.mock2.assert_called_once_with("test2", data_fields.mock1.return_value)
        mock_dummy_data_field_handler.assert_called_once_with("test3", data_fields.mock2.return_value)

    def test_to_dictionary(self):
        data_fields = ConcreteDataFields(["test1", "test2"])
        data_record = MagicMock(spec=DataRecord)

        data_fields.to_dictionary(data_record)
        data_fields.mock2.return_value.add_to_dictionary.assert_called_once_with(data_record, dict())

    def test_get_data_fields(self):
        data_fields = ConcreteDataFields(["test1", "test2"])
        self.assertEqual(data_fields.get_data_fields(), ["test1", "test2"])
