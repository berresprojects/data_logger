from unittest import TestCase

from mock import MagicMock

from data_fields.data_field_handler.dummy_data_field_handler import DummyDataFieldHandler


class TestDummyDataFieldHandler(TestCase):

    def test_init(self):
        DummyDataFieldHandler("test", None)

    def test_add_field(self):
        dummy_data_field_handler = DummyDataFieldHandler("test", None)
        dictionary = dict()

        dummy_data_field_handler.add_field(MagicMock(), dictionary)

        self.assertDictEqual(dictionary, dict(test=None))
