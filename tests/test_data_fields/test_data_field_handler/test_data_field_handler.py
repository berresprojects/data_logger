from unittest import TestCase

from mock import MagicMock

from data_fields.data_field_handler.data_field_handler import DataFieldHandler
from data_record.data_record import DataRecord


class ConcreteDataFieldHandler(DataFieldHandler):

    def add_field(self, data_record, dictionary):
        pass


class TestDataFieldHandler(TestCase):
    def test_get_data_field_name(self):
        data_field_handler = ConcreteDataFieldHandler("test", None)
        self.assertEqual(data_field_handler.get_data_field_name(), "test")

    def test_add_to_dictionary(self):
        successor_mock = MagicMock(spec=ConcreteDataFieldHandler)
        data_field_handler = ConcreteDataFieldHandler("test", successor_mock)
        data_field_handler.add_field = MagicMock()
        data_record = MagicMock(spec=DataRecord)
        dictionary = dict()

        data_field_handler.add_to_dictionary(data_record, dictionary)

        data_field_handler.add_field.assert_called_once_with(data_record, dictionary)
        successor_mock.add_to_dictionary.assert_called_once_with(data_record, dictionary)
