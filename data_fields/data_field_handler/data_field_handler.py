from abc import ABCMeta, abstractmethod


class DataFieldHandler(object):

    __metaclass__ = ABCMeta

    def __init__(self, data_field_name, successor):
        """
        :type data_field_name: str
        :param successor: Next data field to be handled
        :type successor: DataFieldHandler | None
        """
        self.__data_field_name = data_field_name
        self.__successor = successor

    def get_data_field_name(self):
        """
        :rtype: str
        """
        return self.__data_field_name

    def set_successor(self, successor):
        """
        :type successor: DataFieldHandler
        """
        self.__successor = successor

    def add_to_dictionary(self, data_record, dictionary):
        """
        :type data_record: data_record.data_record.DataRecord
        :type dictionary: dict[str, _]
        """
        self.add_field(data_record, dictionary)
        if self.__successor:
            self.__successor.add_to_dictionary(data_record, dictionary)

    @abstractmethod
    def add_field(self, data_record, dictionary):
        """
        :type data_record: data_record.data_record.DataRecord
        :type dictionary: dict[str, _]
        """
