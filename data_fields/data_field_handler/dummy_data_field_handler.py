from data_field_handler import DataFieldHandler


class DummyDataFieldHandler(DataFieldHandler):
    def add_field(self, data_record, dictionary):
        dictionary[self.get_data_field_name()] = None
