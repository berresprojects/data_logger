from abc import ABCMeta, abstractmethod
from logging import getLogger

from data_field_handler.dummy_data_field_handler import DummyDataFieldHandler

logger = getLogger()


class DataFields(object):

    __metaclass__ = ABCMeta

    def __init__(self, parameters_captured):
        """
        :type parameters_captured: list[str]
        """
        self.__data_fields = parameters_captured
        self.__data_field_handler = self.__create_data_field_handlers(parameters_captured)

    @abstractmethod
    def get_data_field_handlers(self):
        """
        :return: dictionary containing the relation between data field names and data field handlers
        :rtype: dict[str, _]
        """

    def to_dictionary(self, data_record):
        dictionary = dict()
        self.__data_field_handler.add_to_dictionary(data_record, dictionary)
        return dictionary

    def get_data_fields(self):
        """
        :rtype parameters_captured: list[str]
        """
        return self.__data_fields

    def __create_data_field_handlers(self, parameters_captured):
        """
        :type parameters_captured: list[str]
        :rtype: src.data_logger.data_field_handler.data_field_handler.DataFieldHandler
        """
        previous_data_field_handler = None
        for parameter_captured in parameters_captured:
            constructor = self.get_data_field_handlers().get(parameter_captured)
            if constructor:
                previous_data_field_handler = constructor(parameter_captured, previous_data_field_handler)
            else:
                previous_data_field_handler = DummyDataFieldHandler(parameter_captured, previous_data_field_handler)
                logger.warning("No data field handler found for {}".format(parameter_captured))
        return previous_data_field_handler
