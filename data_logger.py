from abc import ABCMeta, abstractproperty
from csv import DictWriter
from datetime import datetime
from logging import getLogger

logger = getLogger()


class DataLogger(object):

    __metaclass__ = ABCMeta

    DUMPFILE_DIR = "results"
    DUMPFILE_EXTENSION = "csv"
    DUMPFILE_SEPARATOR = ";"

    def __init__(self, data_fields, data_logger_configuration):
        """
        :param data_fields: DataFields constructor
        :type data_logger_configuration: data_logger_configuration.DataLoggerConfiguration
        """
        self.__data_fields = data_fields(data_logger_configuration.capture)
        self.csv_file = self.__create_csv_file()
        self.data_logger = DictWriter(self.csv_file,
                                      fieldnames=self.__data_fields.get_data_fields(),
                                      delimiter=self.DUMPFILE_SEPARATOR)
        self.data_logger.writeheader()

    def capture(self, data_record):
        """
        :type data_record: data_record.data_record
        """
        self.data_logger.writerow(self.__data_fields.to_dictionary(data_record))

    @property
    def log_directory(self):
        """
        :rtype: str
        """
        return self.DUMPFILE_DIR

    @abstractproperty
    def log_file_name(self):
        """
        :rtype: str
        """

    def destroy(self):
        self.csv_file.close()

    def __create_csv_file(self):
        csv_file = '{target_dir}/DATE_{date}_{configuration}.{extension}'.format(
            target_dir=self.log_directory,
            date=datetime.utcnow().strftime('%d_%m_%Y_%H_%M_%S'),
            configuration=self.log_file_name,
            extension=self.DUMPFILE_EXTENSION)
        return open(csv_file, 'w')
