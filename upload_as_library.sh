LIBRARY_LOCATION='/home/pi/.local/lib/python2.7/site-packages/data_logger/'

ssh $1 'mkdir -p '"$LIBRARY_LOCATION"
ssh $1 'rm -rf '"${LIBRARY_LOCATION}"'/*'
scp -r  ./tests ./data_fields ./data_record __init__.py data_logger_configuration.py data_logger.py  $1:${LIBRARY_LOCATION}
